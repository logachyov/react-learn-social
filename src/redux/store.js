import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";
import sidebarReducer from "./sidebar-reducer";

let store = {
    _callSubscriber() {

    },
    _state: {
        profile: {
            postData: [
                {id: 1, message: 'Hello, World!', likeCount: '10'},
                {id: 2, message: 'How are you?', likeCount: '30'},
            ],
            newPostText: '',
        },
        messages: {
            dialogsData: [
                {id: 1, name: 'Dima', avatar: 'https://i.pinimg.com/originals/0c/a9/e2/0ca9e28dcb12dc698cfd2beda6d6fa64.jpg'},
                {id: 2, name: 'Jenya', avatar: 'https://klike.net/uploads/posts/2019-03/1551511784_4.jpg'},
                {id: 3, name: 'Pit', avatar: 'https://whatsism.com/uploads/posts/2018-07/1530544023_n6fgwzftnvg.jpg'},
                {id: 4, name: 'John', avatar: 'https://www.meme-arsenal.com/memes/cd2652ae9d5c44e3c695d72fd37f647e.jpg'},
                {id: 5, name: 'Sinty', avatar: 'https://author.today/content/2020/01/15/70c3267634e848bdb6bab342a2d126c3.jpg'},

            ],
            messagesData: [
                {id: 1, message: 'Hi!'},
                {id: 2, message: 'How are you?'},
                {id: 3, message: 'Where are you?'},
            ],
            newMessageBody: '',
        },
        sidebar: {
            friend: [
                {id: 1, name: 'Dima', avatar: 'https://i.pinimg.com/originals/0c/a9/e2/0ca9e28dcb12dc698cfd2beda6d6fa64.jpg'},
                {id: 2, name: 'Jenya', avatar: 'https://klike.net/uploads/posts/2019-03/1551511784_4.jpg'},
                {id: 3, name: 'Pit', avatar: 'https://whatsism.com/uploads/posts/2018-07/1530544023_n6fgwzftnvg.jpg'},
            ],
        },
    },

    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._callSubscriber = observer;
    },

    dispatch(action) {
        this._state.profile = profileReducer(this._state.profile, action);
        this._state.messages = dialogsReducer(this._state.messages, action);
        this._state.sidebar = sidebarReducer(this._state.sidebar, action);
        this._callSubscriber(this._state);
    },
};

export default store;
window.store = store;