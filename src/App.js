import React from 'react';
import './App.css';
import Navbar from "./components/Navbar/Navbar";
import {Route} from "react-router-dom";
import DialogsContainer from "./components/Dialogs/DialogsContainer";
import UsersContainer from "./components/Users/UsersContainer";
import ProfileContainer from "./components/Profile/ProfileContainer";
import HeaderContainer from "./components/Header/HeaderContainer";

const App = (props) => {
  return (
      <div className="app-wrapper">
          <div className="container">
              <div className="app-wrap">
                  <HeaderContainer/>
                  <Navbar/>{/* state={props.state.sidebar}*/}
                  <div className="app-wrapper__content ">
                      <Route path="/profile/:userId?"
                             render={() => <ProfileContainer/>}
                      />
                      <Route path="/dialogs"
                             render={() => <DialogsContainer/>}
                      />
                      <Route path="/users"
                             render={() => <UsersContainer/>}
                      />
                  </div>
              </div>
          </div>
      </div>
  );
};

export default App;
