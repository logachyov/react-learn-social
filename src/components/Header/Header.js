import React from 'react';
import h from './Header.module.scss';
import {NavLink} from "react-router-dom";

const Header = (props) => {
    return (
        <div className={h.wrap}>
            <NavLink to="/" className={h.itemLink}>
                <img className={h.itemImg} src="http://logok.org/wp-content/uploads/2014/05/Total-logo-earth-880x660.png" alt="logo"/>
            </NavLink>
            { props.isAuth ? <div className={h.itemLoginActive}>{props.login}</div>
                : <NavLink activeClassName={h.active} className={h.itemLogin} to={'/login'}>Sing In</NavLink>
            }
        </div>

    );
}
export default Header;