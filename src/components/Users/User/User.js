import React from "react";
import user from './User.module.scss';
import {NavLink} from "react-router-dom";

let User = (props) => {

    return (
        <div className={user.wrap}>
            <div className={user.avatar}>
                <NavLink to={'/profile/' + props.id}>
                    <img className={user.avatarImg} src={props.photoUrl} alt=""/>
                </NavLink>
                {props.btn}
            </div>
            <div className={user.info}>
                <div className={user.item}>
                    <div className={user.itemName}>{props.fullName}</div>
                    <div className={user.itemStatus}>{props.status}</div>
                </div>
                <div className={user.location}>
                    <div className={user.locationCountry}>{props.country},</div>
                    <div className={user.locationCity}>{props.city}</div>
                </div>

            </div>
        </div>
    )
};

export default User;