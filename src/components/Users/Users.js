import React from "react";
import User from "./User/User";
import users from './Users.module.scss'
import userPhoto from '../../assets/images/usersnone.png'
import * as axios from "axios";

let Users = (props) => {

        let pagesCount = Math.ceil(props.totalUsersCount / props.pageSize);

        let pages = [ ];
        for ( let i=1; i <= pagesCount; i++) {
            pages.push(i)
        }

        return (
            <div>
                <div>
                    {pages.map( p => {
                        return <span onClick={ (e) => {props.onPageChanged(p)}}
                                     className={props.currentPage === p && users.selectedPage}>{p}</span>
                    })}
                </div>
                {
                    props.users.map( u =>
                        <User key={u.id}
                              fullName={u.name}
                              status={u.status}
                              id={u.id}
                              country={'u.location.country'}
                              city={'u.location.city'}
                              photoUrl={u.photos.small != null ? u.photos.small : userPhoto}
                              btn={u.followed
                                  ? <button disabled={props.followingInProgress.some(id => id === u.id)} className={users.avatarUnfollow}
                                            onClick={ () => {
                                                props.toogleFollowingProgress(true, u.id);
                                                axios.delete(`https://social-network.samuraijs.com/api/1.0/follow/${u.id}`, {
                                                    withCredentials: true,
                                                    headers: {
                                                        "API-KEY": "9f71b8aa-d670-42ed-bb1b-600c3573e7ab"
                                                    }
                                                }).then(response => {
                                                        if (response.data.resultCode === 0) {
                                                            props.unfollow(u.id)
                                                        }
                                                        props.toogleFollowingProgress(false, u.id);
                                                    })

                                  }}>Unfollow</button>
                                  : <button disabled={props.followingInProgress.some(id => id === u.id)} className={users.avatarFollow}
                                            onClick={ () => {
                                                props.toogleFollowingProgress(true, u.id);
                                                axios.post(`https://social-network.samuraijs.com/api/1.0/follow/${u.id}`, {}, {
                                                    withCredentials: true,
                                                    headers: {
                                                        "API-KEY": "9f71b8aa-d670-42ed-bb1b-600c3573e7ab"
                                                    }
                                                }).then(response => {
                                                        if (response.data.resultCode === 0) {
                                                            props.follow(u.id)
                                                        }
                                                        props.toogleFollowingProgress(false, u.id);
                                                    })
                                  }}>Follow</button>}
                        />)
                }
            </div>
        )
}

export default Users;