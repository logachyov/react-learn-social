import React from 'react';
import preloaderSVG from '../../../assets/images/preloader.svg'

let Preloader = (props) => {
    return <div>
        <img src={preloaderSVG} alt="Preloading"/>
    </div>
}

export default Preloader;