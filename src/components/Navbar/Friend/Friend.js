import React from 'react';
import n from './Friend.module.scss';

const Friend = (props) => {

    return (
        <div className={n.friendItem}>
            <img className={n.imgItem} src={props.avatar} alt=""/>
            <div className={n.nameItem}>{props.name}</div>
        </div>
    );
};

export default Friend;