import React from 'react';
import n from './Navbar.module.scss';
import {NavLink} from "react-router-dom";
//import Friend from "./Friend/Friend";

const Navbar = (props) => {

    //let friendElements = props.state.friend.map( friend =>  <Friend name={friend.name} avatar={friend.avatar} id={friend.id}/> );

    return (
        <nav className={n.wrap}>
                <div className={n.item}>
                    <NavLink className={n.link} activeClassName={n.active} to="/profile">Profile</NavLink>
                </div>
                <div className={n.item}>
                    <NavLink className={n.link} activeClassName={n.active} to="/dialogs">Messages</NavLink>
                </div>
                <div className={n.item}>
                    <NavLink className={n.link} activeClassName={n.active} to="/3">News</NavLink>
                </div>
                <div className={n.item}>
                    <NavLink className={n.link} activeClassName={n.active} to="/4">Music</NavLink>
                </div>
                <div className={n.item}>
                    <NavLink className={n.link} activeClassName={n.active} to="/users">Find users</NavLink>
                </div>
                <div className={n.item}>
                    <NavLink className={n.link} activeClassName={n.active} to="/5">Settings</NavLink>
                </div>
                <div className={n.item}>
                    <NavLink className={n.link} activeClassName={n.active} to="/6">Friends</NavLink>
                    <div className={n.friendWrap}>
                        {/*{friendElements}*/}
                    </div>
                </div>
        </nav>
    );
}

export default Navbar;