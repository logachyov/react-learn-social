import React from 'react';
import p from './Dialog.module.scss';
import {NavLink} from "react-router-dom";

const Dialog = (props) => {

    let path = "/dialogs/" + props.id;

    return (
        <div className={p.dialog}>
            <img className={p.imgItem} src={props.avatar} alt=""/>
            <NavLink className={p.link} activeClassName={p.active} to={path}>{props.name}</NavLink>
        </div>
    );
};

export default Dialog;