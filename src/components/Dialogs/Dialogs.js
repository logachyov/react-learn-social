import React from 'react';
import p from './Dialogs.module.scss';
import Dialog from "./Dialog/Dialog";
import Message from "./Message/Message";
//import MessageForm from "./Message/MessageForm/MessageForm";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons/faEnvelope";

const Dialogs = (props) => {

    let state = props.messages;

    let dialogsElements = state.dialogsData.map( dialog =>  <Dialog name={dialog.name} avatar={dialog.avatar} key={dialog.id} id={dialog.id}/> );
    let messagesElements = state.messagesData.map( message => <Message message={message.message} key={message.id} id={message.id}/> );
    let newMessageBody = state.newMessageBody;

    let onSendMessageClick = () => {
        props.sendMessage();
    };
    let onNewMessageChange = (e) => {
        let body = e.target.value;
        props.updateNewMessageBody(body);
    };

    return (
        <div>
            <div className={p.title}>Dialogs</div>
            <div className={p.dialogs}>
                <div className={p.dialogsItem}>
                    {dialogsElements}
                </div>
                <div className={p.messages}>
                    {messagesElements}
                    <div className={p.messagesItem}>
                        <textarea
                        className={p.messagesFormText}
                        value={newMessageBody}
                        onChange={onNewMessageChange}
                        placeholder={"Your messages ..."}
                        id=""
                        rows="2"
                        />
                        <button className={p.messagesFormSend} onClick={onSendMessageClick}>
                            <FontAwesomeIcon className={p.icon} icon={faEnvelope}/>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Dialogs;