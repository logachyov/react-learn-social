import React from 'react';
import p from './MessageForm.module.scss';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons/faEnvelope";
import {sendMessageCreator, updateNewMessageBodyCreator} from "../../../../redux/dialogs-reducer";

const MessageForm = (props) => {

    let newMessageBody = props.state.newMessageBody;

    let onSendMessageClick = () => {
        props.store.dispatch(sendMessageCreator())
    };
    let onNewMessageChange = (e) => {
        let body = e.target.value;
        props.store.dispatch(updateNewMessageBodyCreator(body))
    }

    return (
        <div className={p.item}>
            <textarea
                className={p.form__text}
                value={newMessageBody}
                onChange={onNewMessageChange}
                placeholder={"Your messages ..."}
                id=""
                rows="2"
            />
            <button className={p.btn} onClick={onSendMessageClick}>
                <FontAwesomeIcon className={p.icon} icon={faEnvelope}/>
            </button>
        </div>
    );
};

export default MessageForm;