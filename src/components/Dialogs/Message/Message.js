import React from 'react';
import p from './Message.module.scss';

const Message = (props) => {
    return (
        <div>
            <div className={p.message}>{props.message}</div>
        </div>
    );
};

export default Message;