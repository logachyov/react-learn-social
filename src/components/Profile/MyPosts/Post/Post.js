import React from 'react';
import m from './Post.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faHeart} from "@fortawesome/free-solid-svg-icons/faHeart";

const Post = (props) => {
    return (
            <div className={m.item}>
                <img className={m.avatar} src="https://pm1.narvii.com/6889/74979d4d2744ec6e27995b6e866f091d04c0b40cr1-515-414v2_uhq.jpg" alt=""/>
                <div className={m.text}>
                    { props.message }
                </div>
                <div className={m.like}>
                    <FontAwesomeIcon className={m.icon} icon={faHeart}/>
                    { props.likeCount }
                </div>
            </div>
    );
}
export default Post;