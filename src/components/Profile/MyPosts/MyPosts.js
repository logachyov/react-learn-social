import React from 'react';
import m from './MyPosts.module.scss';
import Post from "./Post/Post";

const MyPosts = (props) => {

    let postElement = props.postData.map( p =>
        <Post message={p.message} key={p.id} id={p.id} likeCount={p.likeCount}/> );

    let newPostElement = React.createRef();

    let onAddPost = () => {
       props.addPost();
    };

    let onPostChange = () => {
        let text= newPostElement.current.value;
        props.updateNewPostText(text);
    };

    return (
        <div className={m.wrap}>
            <div className={m.title}>My post</div>
            <div className={m.formItem}>
                <textarea
                    className={m.formText}
                    onChange={onPostChange}
                    value={props.newPostText}
                    ref={newPostElement}
                    placeholder={"Your messages ..."}
                    id=""
                    rows="4"
                />
                <button className={m.formSend} onClick={onAddPost}>Send</button>
            </div>
            { postElement }
        </div>
    );
};

export default MyPosts;