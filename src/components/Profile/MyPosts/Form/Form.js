import React from 'react';
import m from './Form.module.scss';
import {addPostActionCreator, updateNewPostTextActionCreator} from "../../../../redux/profile-reducer";

const Form = (props) => {

    let newPostElement = React.createRef();

    let onAddPost = () => {
        props.dispatch(addPostActionCreator());
    };

    let onPostChange = () => {
        let text= newPostElement.current.value;
        let action = updateNewPostTextActionCreator(text);
        props.dispatch(action);
    };

    return (
            <div className={m.item}>
                <textarea
                    className={m.form__text}
                    onChange={onPostChange}
                    value={props.newPostText}
                    ref={newPostElement}
                    placeholder={"Your messages ..."}
                    id=""
                    rows="4"
                />
                <button className={m.btn} onClick={onAddPost}>Send</button>
            </div>
    );
};

export default Form;