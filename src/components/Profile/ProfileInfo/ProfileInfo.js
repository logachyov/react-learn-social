import React from 'react';
import p from './ProfileInfo.module.scss';
import Preloader from "../../common/Preloader/Preloader";

const ProfileInfo = (props) => {
    if (!props.profile) {
        return <Preloader/>
    }

    return (
        <div>
            <div className={p.content__img}>
                <img className={p.img__item} src="https://jssors8.azureedge.net/demos/image-slider/img/px-beach-daylight-fun-1430675-image.jpg" alt=""/>
            </div>
            <div className={p.info}>
                <img className={p.info__img} src={props.profile.photos.large} alt=""/>
                <div className={p.item}>
                    <div className={p.name}>{props.profile.fullName}</div>
                    <div className={p.item__info}>
                        <span className={p.item__content}>Date of Birth: </span>
                        <span className={p.item__content}>City: </span>
                        <span className={p.item__content}>Education: </span>
                        <span className={p.item__content}>Web-site: {props.profile.contacts.vk}</span>
                        <span className={p.item__content}>About Me: {props.profile.aboutMe}</span>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default ProfileInfo;